### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:54:29 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


# Evalue values used for the filtering
EVAL ?= 1e-04
# name of the plot to be printed
SATURATION_PLOT ?= saturation_plot.pdf
# Use polynomial model for fitting
SATURATION_PLOT_POLYNOMIAL ?= saturation_plot_polynomial.pdf
# Initial parameter A for the fitting
# of the non-linear model 
NLM_A_PARAM ?= 1000
# Initial parameter B for the fitting
# of the non-linear model 
NLM_B_PARAM ?= 6000
# The contig in which each reads was assembled is used as support for the BLAST search against the proteome.
CONTIG_READ_LIST ?=
# title for the data in saturation plot legend
PLOT_DATA_LEGEND ?=


# I need blast_fields.py
context prj/annotation


# Include external files as variables
# If files are not present it search for a rules.mk file
# and only execute the rules needed to create the target files
extern ../../phase_1/assembled_reads.txt as ASSEMBLED_READS_LST
extern ../../phase_1/external_rules.mk as EXTERNAL_RULES
extern ../../phase_1/assembly_blast.xml as ASSEMBLY_BLAST
extern ../../phase_1/proteome/proteome as PROTEOME_LN


include $(EXTERNAL_RULES)


contig_read_list.lst: $(CONTIG_READ_LIST)
	ln -sf $< $@





# add a suffix before each element of $(SIZES). Then add to such composition
# a prefix = the file name. 
SAMPLES_READS_LST        := $(addprefix sample_reads_lst_,$(addsuffix .txt, $(SIZES)))

# MAP
#       -ONE FILE AS PREREQUISITE, 
#       -MANY FILES AS TARGETS

# $ * indicates the steam of the pattern 
# identified by "%"

# Extract a sample of random elements from the column, of a given sample size
# 1) radomize the column
# 2) extract the last N elements (the given sample size)
# the use of head causes an error, so use tail.
sample_reads_lst_%.txt: $(ASSEMBLED_READS_LST)
	randomize_column 1 < $< \
	| tail --lines $* > $@



# All sample_contig_lst_$(SIZES).txt
SAMPLES_CONTIGS_LST	 := $(addprefix sample_contigs_lst_,$(addsuffix .txt, $(SIZES)))
# For each read, extract the contig in which it was assembled
sample_contigs_lst_%.txt: contig_read_list.lst sample_reads_lst_%.txt
	grep --fixed-strings --word-regexp --file $^2 < $< \
	| cut -f 1 \
	| bsort --key=1,1 \
	| uniq > $@



# From the blast of all contigs against given proteome (xml), extract contig names that
# have match below give e-value and corresponding best hit names 
blast_results.tab: $(ASSEMBLY_BLAST)
	blast_fields -f --evalue-filter $(EVAL) -r query -a hit_def < $< > $@



# All sample_blast_filtered_$(SIZES).txt
SAMPLES_BLAST_FILTERED      := $(addprefix sample_blast_filtered_,$(addsuffix .txt, $(SIZES)))

# From the list of all contigs that have a match, extracts those that appear in 
# the list sample_contigs_lst_%.txt (contigs from the sample size).
# Best hits are added to common contigs.
sample_blast_filtered_%.txt: blast_results.tab sample_contigs_lst_%.txt
	comm -1 -2 \
	<(cut -f 1 $< | bsort --key=1,1) \
	<(bsort --key=1,1 $^2) \
	| translate --append $< 1 > $@








# All uniqueID_sample_filtered_$(SIZES).txt
UNIQUEID_SAMPLE_FILTERED    := $(addprefix uniqueID_sample_filtered_,$(addsuffix .txt, $(SIZES)))



# Take a list of unique contigs and corresponding best hits.
# Reduce lines by unique Ensembl gene stable IDs (in the hit).

# To call this function use $(call unique_hit_counter, $<,repeat_upper_limit, 
# repeat_lower_limit, target_file).

# The filter use an interval of [lower limit repetition : upper limit repetition] 
# in order to report lines.
# Extreme are inclusive.
# It is possible to use $(call unique_hit_counter, $<,1,inf, $@) to avoid upper limit
# It is possible to use $(call unique_hit_counter, $<,-inf,4, $@) to avoid lower limit

# 1) list of Ensembl gene stable IDs is created
# 2) list is sorted
# 3) only unique lines prefixed by number of occurrences are reported
# 4) extra spaces added at the beginning of lines by uniq -c are removed, only 1 
#    is leaved; then spaces are converted to tab
# 6) only lines with occurences within the interval are printed
# 7) translate unique Ensembl gene stable IDs with corresponding query and best hit.
#    For this a dictionary composed by Ensembl gene stable ID, contig name, best hit
#    is created. Since keys are the Ensembl gene stable IDs, duplicated keys may 
#    appear. So duplicated keys in DICTIONARY are allowed.
define unique_hit_counter
	REPEAT_UP_LIMIT="$3" \
	&& REPEAT_LOW_LIMIT="$2" \
	&& bawk '!/^[$$,\#+]/ { \
	split($$2,a," "); \
	split(a[4],b,":"); \
	print b[2]; \
	} ' $1 \
	| bsort \
	| uniq -c \
	| bsort -n \
	| tr -s ' ' \\t \
	| bawk -v repeat_up=$$REPEAT_UP_LIMIT -v repeat_low=$$REPEAT_LOW_LIMIT \
	'!/^[$$,\#+]/ { \
	if (repeat_up == "inf") \
	{ repeat_up="inf"; } \
	if ($$2 >= repeat_low && $$2 <= repeat_up) \
	{ print $$2, $$3; } \
	} ' \
	| translate -d -g $$'\t' --append <( \
	bawk '!/^[$$,\#+]/ { \
	split($$2,a," "); \
	split(a[4],b,":"); \
	print b[2], $$0; \
	} ' $1 ) 2 > $4
endef


# Take a list of unique contigs and corresponding best hits.
# Reduce lines by unique Ensembl gene stable IDs (in the hit).
uniqueID_sample_filtered_%.txt: sample_blast_filtered_%.txt
	 $(call unique_hit_counter, $<,1,inf, $@)



# Reduce lines by Ensembl gene stable IDs that appear at least 5 times
5_UNIQUEID_SAMPLE_FILTERED    := $(addprefix 5_uniqueID_sample_filtered_,$(addsuffix .txt, $(SIZES)))
5_uniqueID_sample_filtered_%.txt: sample_blast_filtered_%.txt
	$(call unique_hit_counter, $<,5,inf, $@)

# Reduce lines by Ensembl gene stable IDs that appear at least 10 times
10_UNIQUEID_SAMPLE_FILTERED    := $(addprefix 10_uniqueID_sample_filtered_,$(addsuffix .txt, $(SIZES)))
10_uniqueID_sample_filtered_%.txt: sample_blast_filtered_%.txt
	$(call unique_hit_counter, $<,10,inf, $@)

# Reduce lines by Ensembl gene stable IDs that appear at least 100 times
100_UNIQUEID_SAMPLE_FILTERED    := $(addprefix 100_uniqueID_sample_filtered_,$(addsuffix .txt, $(SIZES)))
100_uniqueID_sample_filtered_%.txt: sample_blast_filtered_%.txt
	$(call unique_hit_counter, $<,100,inf, $@)





# MAP REDUCTION FUNCTION:
#     -MANY FILES AS PREREQUISITES, 
#     -ONE FILE AS A TARGET

# For each reads sample size get unique Ensembl gene stable IDs found.
# Unique Ensembl gene stable IDs found are number of lines of the corresponding
# uniqueID_sample_filtered_(sample_size).txt
saturation_table.txt: $(UNIQUEID_SAMPLE_FILTERED)
	@echo -e "# list_file_name\tsimple_size\tunique_genes_found" > $@;
	@for i in $(SIZES); do \
	bawk '/[^[:space:]]/ {++x} END {if (x == 0) print FILENAME,'$$i',0; else \
        print FILENAME,'$$i',x}' uniqueID_sample_filtered_$$i.txt; \
	done >> $@ # all non-blank lines

# same as above with Ensembl gene stable IDs found at least 5 times
5_saturation_table.txt: $(5_UNIQUEID_SAMPLE_FILTERED)
	@echo -e "# list_file_name\tsimple_size\tunique_genes_found" > $@;
	@for i in $(SIZES); do \
	bawk '/[^[:space:]]/ {++x} END {if (x == 0) print FILENAME,'$$i',0; else \
        print FILENAME,'$$i',x}' 5_uniqueID_sample_filtered_$$i.txt; \
	done >> $@ # all non-blank lines

# same as above with Ensembl gene stable IDs found at least 10 times
10_saturation_table.txt: $(10_UNIQUEID_SAMPLE_FILTERED)
	@echo -e "# list_file_name\tsimple_size\tunique_genes_found" > $@;
	@for i in $(SIZES); do \
	bawk '/[^[:space:]]/ {++x} END {if (x == 0) print FILENAME,'$$i',0; else \
        print FILENAME,'$$i',x}' 10_uniqueID_sample_filtered_$$i.txt; \
	done >> $@ # all non-blank lines

# same as above with Ensembl gene stable IDs found at least 100 times
100_saturation_table.txt: $(100_UNIQUEID_SAMPLE_FILTERED)
	@echo -e "# list_file_name\tsimple_size\tunique_genes_found" > $@;
	@for i in $(SIZES); do \
	bawk '/[^[:space:]]/ {++x} END {if (x == 0) print FILENAME,'$$i',0; else \
        print FILENAME,'$$i',x}' 100_uniqueID_sample_filtered_$$i.txt; \
	done >> $@ # all non-blank lines







# Use the data for the saturation curve and fit on them the nonlinear model 
# y=(x*a)/(b+x), plot the data and the model on file and extract the a, b 
# coefficients and the slope on last x data.

# Slope of the saturation curve is calculated at the end of the curve.
# It is the value of the derivative of the function, replacing x=last(simple_size).

# FARE IN MODO CHE AUTOSTIMI I PARAMETRI INIZIALI
PARAMETERS_FILE = nl_model_parameters.txt
fitted_table.txt: saturation_table.txt
	paste $< \
	<(fit_nl_model --a-param $(NLM_A_PARAM) --b-param $(NLM_B_PARAM) --param-out-file $(PARAMETERS_FILE) \
	--row-names 1,2 < $< \
	| cut -f 3 \
	| awk 'NR==1{printf "fitted_unique_genes_found"}1') > $@

$(PARAMETERS_FILE): fitted_table.txt
	touch $<


# print plot to file with fitted values and model curve
$(SATURATION_PLOT): fitted_table.txt $(PARAMETERS_FILE)
	plot_nl_model -o $@ -s -t "saturation curve" -x "reads sampled [#]" -y "different genes identified [#]" -q -d "$(PLOT_DATA_LEGEND)","model: " -c "blue","red" -p $^2,$^2 1,2 1,3 < $<





# polynomial_model_parameters.txt: 5_saturation_table.txt 10_saturation_table.txt 100_saturation_table.txt 
# 	fit_polynomial_model -r $(RNA_SOURCE),$(RNA_SOURCE),$(RNA_SOURCE) \
# 	-p $(PROTEOME_SOURCE),$(PROTEOME_SOURCE),$(PROTEOME_SOURCE) \
# 	-o $(SATURATION_PLOT_POLYNOMIAL) > $@ $< $^2 $^3 






LAST_UNIQUEID_SAMPLE_FILTERED = $(call last,$(UNIQUEID_SAMPLE_FILTERED))
# For testing
.PHONY: test_new_function
test_new_function:
	@echo $(LAST_SAMPLES_BLAST_FILTERED)
	@echo $(LAST_UNIQUEID_SAMPLE_FILTERED)




# Adds the valus to the model fitting parameter file:
# total genes in proteome	
# max number of genes discovered
# total transcript in the proteome
# max number of transcript discovered

# Get last element from list, from GNU MAKE STANDARD LIBRARY
# different contigs with hits (and respective best hits) at maximum reads sample size
LAST_SAMPLES_BLAST_FILTERED = $(call last,$(SAMPLES_BLAST_FILTERED))
nl_model_parameters_plus.txt: nl_model_parameters.txt $(PROTEOME_LN) saturation_table.txt $(LAST_SAMPLES_BLAST_FILTERED)
	paste $< \
	<(fasta2tab < $^2 \
	| bawk '!/^[$$,\#+]/ { \
	split($$1,a," "); \
	split(a[4],b,":"); \
	print b[2]; \
	} ' \
	| bsort \
	| uniq -c \
	| bsort -n \
	| wc -l \
	| bawk '!/^[$$,\#+]/ { \
	printf "total_db_genes\n%i\n", $$0; \
	} ') \
	<( \
	tail --lines 1 $^3 \
	| cut -f 3 \
	| bawk '!/^[$$,\#+]/ { \
	printf "db_genes_discovered\n%i\n", $$0; \
	} ' \
	) \
	<(fasta2tab < $^2 \
	| bawk '!/^[$$,\#+]/ { \
	split($$1,a," "); \
	split(a[5],b,":"); \
	print b[2]; \
	} ' \
	| bsort \
	| uniq -c \
	| bsort -n \
	| wc -l \
	| bawk '!/^[$$,\#+]/ { \
	printf "total_db_transcript\n%i\n", $$0; \
	} ') \
	<( \
	bawk '!/^[$$,\#+]/ { \
	split($$2,a," "); \
	split(a[5],b,":"); \
	print b[2]; \
	} ' $^4 \
	| bsort \
	| uniq -c \
	| bsort -n \
	| wc -l \
	| bawk '!/^[$$,\#+]/ { \
	printf "db_transcript_discovered\n%i\n", $$0; \
	} ') > $@








# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += $(SAMPLES_READS_LST) \
	 $(SAMPLES_CONTIGS_LST) \
	 $(SAMPLES_BLAST_FILTERED) \
	 $(UNIQUEID_SAMPLE_FILTERED) \
	 $(5_UNIQUEID_SAMPLE_FILTERED) \
	 $(10_UNIQUEID_SAMPLE_FILTERED) \
	 $(100_UNIQUEID_SAMPLE_FILTERED) \
	 saturation_table.txt \
	 $(SATURATION_PLOT) \
	 5_saturation_table.txt \
	 10_saturation_table.txt \
	 100_saturation_table.txt \
	 nl_model_parameters.txt \
	 nl_model_parameters_plus.txt \
	 blast_results.tab
	 #polynomial_model_parameters.txt \



# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=



# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += gmsl \
	 __gmsl \
	 $(SAMPLES_READS_LST) \
	 $(SAMPLES_CONTIGS_LST) \
	 $(SAMPLES_BLAST_FILTERED) \
	 $(UNIQUEID_SAMPLE_FILTERED) \
	 $(5_UNIQUEID_SAMPLE_FILTERED) \
	 $(10_UNIQUEID_SAMPLE_FILTERED) \
	 $(100_UNIQUEID_SAMPLE_FILTERED) \
	 saturation_table.txt \
	 5_saturation_table.txt \
	 10_saturation_table.txt \
	 100_saturation_table.txt \
	 nl_model_parameters.txt \
	 $(SATURATION_PLOT) \
	 $(SATURATION_PLOT_POLYNOMIAL) \
	 nl_model_parameters_plus.txt \
	 blast_results.tab \
	 contig_read_list.lst \
	 fitted_table.txt
	 #polynomial_model_parameters.txt \




# Declare clean as
# phony targhet
.PHONY: clean_dir
# so i can recall function to
# delete dir or other files
clean_dir:


######################################################################
### phase_2.mk ends here
