### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# ftp address of the fasta file for the proteome from ensembl
PROTEOME ?=
# ftp address of the corresponding README file
README ?= 

# Reference proteome against which to pursue the BLAST
ASSEMBLY ?=
# List of contig-reads for get information about witch contig contains each reads
READS ?=
# I want to get a number of samples = NUMBER_SAMPLING, 
# so i have to do NUMBER_SAMPLING cycles of sampling.
NUMBER_SAMPLING ?=
# Number of threads to use with ncbi-blast
BLAST_THREADS ?=




# create links for all the necessary files
contig_read_lst_ln.txt: $(CONTIG_READ_LIST)
	ln -sf $(CONTIG_READ_LIST) $@

reads_ln.fasta: $(READS)
	ln -sf $(READS) $@

assembly_ln.fasta: $(ASSEMBLY)
	ln -sf $(ASSEMBLY) $@


# download reference proteome and transform to blast database
proteome.flag:
	$(call download_DB, $(PROTEOME), $(README), $@)

define download_DB
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	_comment "protein sequence downloaded from ensembl release 66" && \
	wget -q -O $(basename $3).gz $1 && \
	wget -q  $2 && \
	gunzip $(basename $3).gz && \
	makeblastdb -in $(basename $3) -dbtype prot -title $(basename $3) && \
	cd .. && \
	touch $3
endef




# Extract the list of all assembled reads from contig_read_lst_ln.txt
# avoiding comments and blank lines
ASSEMBLED_READS_LST := assembled_reads.txt
$(ASSEMBLED_READS_LST): reads_ln.fasta
	fasta2tab < $< \
	| bawk '!/^[$$,\#+]/ { print $$1 }' > $@




# Generate external rule that assign variable SIZES:= ...
external_rules.mk: $(ASSEMBLED_READS_LST)
	_comment () { echo -n ""; }; \
	echo "Calculating list of simple sizes..."; \
	( \
	NUMBER_SAMPLING=$(NUMBER_SAMPLING); \
	_comment "counts the number of lines in the file assembled_reads.fasta"; \
	READS_SIZE=`bawk '!/^[$$,\#+]/ {++x} END {print x}' $<`; \
	_comment "prints the result of a division, for which is calculated the integer part"; \
	NUM_SAMPLES=`echo $$(($$READS_SIZE/$$NUMBER_SAMPLING))`; \
	LAST_SIZE=`echo "$$READS_SIZE - $$NUM_SAMPLES" | bc`; \
	echo -n 'SIZES := ' ; \
	_comment "I stop in the penultimate value of increments, and add the maximum number of reads"; \
	SIZES=`seq -s ' ' $$NUM_SAMPLES $$NUM_SAMPLES $$LAST_SIZE | paste -d ' ' - <(echo $$READS_SIZE)` ; \
	echo -e "$$SIZES\n" ; \
	) >$@

# assign variable SIZES:= ...
include external_rules.mk




# Allignment of alla the assembly against all the proteome
ASSEMBLY_BLAST := assembly_blast.xml
# If you mark the file as precious, make
# will never delete the file if interrupted.
.PRECIOUS: $(ASSEMBLY_BLAST)

# check if the file assembly_blast.xml is present or not. 
# If it is, it updates its timestamp if it is not, create it.
ifeq ($(findstring $(ASSEMBLY_BLAST),$(wildcard *.xml)), )
# Allignment of alla the assembly against all the proteome
$(ASSEMBLY_BLAST): assembly_ln.fasta proteome.flag
	@echo -e "\nExecuting Blast..\n"; \
	blastx -db $(basename $^2)/$(basename $^2) -query $< -evalue 10 \
	-num_threads $(BLAST_THREADS) -outfmt 5 -max_target_seqs 20 -out $@
else
$(ASSEMBLY_BLAST):
	@echo -e "\nTouching precedent $(ASSEMBLY_BLAST)...\n"; \
	touch $@
endif













# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += contig_read_lst_ln.txt \
	 proteome.flag \
	 reads_ln.fasta \
	 assembly_ln.fasta \
	 $(ASSEMBLED_READS_LST) \
	 $(ASSEMBLY_BLAST) 


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += proteome.flag \
	 assembly_ln.fasta \
	 contig_read_lst_ln.txt \
	 reads_ln.fasta \
	 $(ASSEMBLED_READS_LST) \
	 external_rules.mk

# Declare clean as
# phony targhet
.PHONY: clean_dir
# so i can recall function to
# delete dir or other files
clean_dir:
	 $(RM) -r proteome




######################################################################
### phase_1.mk ends here
